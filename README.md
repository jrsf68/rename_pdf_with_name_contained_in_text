# Rename PDF file with name contained in PDF text itself.

## Problem
Given a PDF file
When locate a specific name in your text
Then rename the PDF file with this name

## Application
### Use case
Given a list of names and a folder with PDF files
When searching each name in each file in the folder
Then rename the PDF file with the name that is in the text of the file


## Domain 

### Entity (schema)
pdf_file = {
    name: string
    pages: list[page]
}

### Methods (implementation)
- open PDF file
- read page from PDF file
- find specific string in page
- rename PDF file 
