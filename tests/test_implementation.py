"""Test methods of application's domain 
"""

from app.domain.implementation import find_specific_string_in_page, open_PDF_file, read_page_from_PDF_file, rename_PDF_file


def test_open_PDF_file():
    pdf_file_name = '/home/ricardo/Documentos/python/rename_pdf_with_name_contained_in_text/app/infra/repository/CONTROLE FREQUENCIA PRODEPA-1.pdf'

    handler_pdf_file = open_PDF_file(pdf_file_name)

    assert handler_pdf_file


def test_read_page_from_PDF_file():
    pdf_file_name = '/home/ricardo/Documentos/python/rename_pdf_with_name_contained_in_text/app/infra/repository/CONTROLE FREQUENCIA PRODEPA-1.pdf'
    handler_pdf_file = open_PDF_file(pdf_file_name)

    page = read_page_from_PDF_file(handler_pdf_file, number_page=0)

    assert page


def test_find_specific_string_in_page():
    text_find = '1 2 3 de Oliveria Quatro'
    pdf_file_name = '/home/ricardo/Documentos/python/rename_pdf_with_name_contained_in_text/app/infra/repository/CONTROLE FREQUENCIA PRODEPA-1.pdf'
    handler_pdf_file = open_PDF_file(pdf_file_name)
    page = read_page_from_PDF_file(handler_pdf_file, number_page=0)

    text_found = find_specific_string_in_page(page, text_find)

    assert text_found != -1


def test_rename_PDF_file():
    new_name = 'Pacifico Armando Guerra'
    pdf_file_name = '/home/ricardo/Documentos/python/rename_pdf_with_name_contained_in_text/app/infra/repository/Pacifico Armando Guerra.pdf'
    handler_pdf_file = open_PDF_file(pdf_file_name)
    page = read_page_from_PDF_file(handler_pdf_file, number_page=0)
    text_found = find_specific_string_in_page(page, new_name)

    renamed_pdf_file = rename_PDF_file(new_name, pdf_file_name)

    assert renamed_pdf_file
