'''Test of use case application
'''

from app.application.use_case_rename_file import get_name_list, read_list_of_PDF_files_from_the_folder, rename_PDF_file_with_name_that_is_in_text, search_names_in_files


def test_list_of_names_is_populated():
    name_list = ['Amin Amou Amado', 'Antonio Manso de Oliveira Sossegado']
    list_name_txt_file = '/home/ricardo/Documentos/python/rename_pdf_with_name_contained_in_text/app/infra/repository/list_of_names.txt'

    name_list_read = get_name_list(list_name_txt_file)

    assert set(name_list_read) == set(name_list)


def test_read_list_of_PDF_files_from_the_folder():
    folder = '/home/ricardo/Documentos/python/rename_pdf_with_name_contained_in_text/app/infra/repository/folder/'

    list_of_PDF_files = read_list_of_PDF_files_from_the_folder(folder)

    assert list_of_PDF_files


def test_search_names_in_files():
    name_list = ['Amin Amou Amado', 'Antonio Manso de Oliveira Sossegado']
    folder = '/home/ricardo/Documentos/python/rename_pdf_with_name_contained_in_text/app/infra/repository/folder/'
    prefix = 'FREQ-MAR-2023 - '
    list_of_PDF_files = read_list_of_PDF_files_from_the_folder(folder)

    match_name = search_names_in_files(folder, prefix, list_of_PDF_files, name_list)

    assert match_name


def test_rename_PDF_file_with_name_that_is_in_text():
    name_list = ['Amin Amou Amado', 'Antonio Manso de Oliveira Sossegado']
    folder = '/home/ricardo/Documentos/python/rename_pdf_with_name_contained_in_text/app/infra/repository/folder/'
    prefix = 'FREQ-MAR-2023 - '
    list_of_PDF_files = read_list_of_PDF_files_from_the_folder(folder)
    match_name = search_names_in_files(folder, prefix, list_of_PDF_files, name_list)

    renamed_pdf = rename_PDF_file_with_name_that_is_in_text(match_name)

    assert renamed_pdf
