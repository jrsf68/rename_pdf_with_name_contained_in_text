"""Application domain methods

"""

from os import rename
from pypdf import PdfReader


def open_PDF_file(pdf_file_name: str):
    try:
        handler_pdf_file = PdfReader(pdf_file_name)
        return handler_pdf_file
    except:
        raise Exception(f'Error to open file {pdf_file_name}')
    

def read_page_from_PDF_file(handler_pdf_file, number_page):
    page = handler_pdf_file.pages[number_page]
    return page


def find_specific_string_in_page(page, text_find):
    text = page.extract_text()
    text_found = text.find(text_find)
    if text_found != -1:
        return True
    else:
        return False


def rename_PDF_file(new_name, pdf_file_name):
    pdf_file_name_splited = pdf_file_name.split('/')
    pdf_file_name_splited = pdf_file_name_splited[-1].split('.pdf')
    new_pdf_name = pdf_file_name.replace(pdf_file_name_splited[0], new_name)
    try:
        rename(pdf_file_name, new_pdf_name)
        return True
    except:
        raise Exception(f'Error rename file {pdf_file_name} to {new_pdf_name}')
    return False
