'''use case application
'''

from re import sub
from os import listdir

from app.domain.implementation import find_specific_string_in_page, open_PDF_file, read_page_from_PDF_file, rename_PDF_file


def get_name_list(list_name_txt_file):
    try:
        with open(list_name_txt_file, 'r') as txt_file:
            name_list = txt_file.readlines()
            name_list = [sub('\n', '', item) for item in name_list]
        return name_list
    except:
        raise(f'Error open file {list_name_txt_file}')
    return False


def read_list_of_PDF_files_from_the_folder(folder):
    list_of_PDF_files = listdir(folder)
    return list_of_PDF_files


def search_names_in_files(folder, prefix, list_of_PDF_files, name_list):
    name_matches_the_PDF_file = {}
    if name_list is None or list_of_PDF_files is None:
        return False
    for name in name_list:
        for pdf in list_of_PDF_files:
            try:
                handler_pdf_file = open_PDF_file(folder + pdf)
            except:
                raise Exception(f'Error open file {pdf}')
            page = None
            number_page = 0
            while not page and number_page <= len(handler_pdf_file.pages):
                page = read_page_from_PDF_file(handler_pdf_file, number_page)
                if find_specific_string_in_page(page, name):
                    name_matches_the_PDF_file[prefix + name] = folder + pdf
                    break
            if name in name_matches_the_PDF_file.keys():
                if name_matches_the_PDF_file[prefix + name] == folder + pdf:
                    break
    return name_matches_the_PDF_file


def rename_PDF_file_with_name_that_is_in_text(match_name):
    if not match_name:
        return False
    renamed = False
    for new_name in match_name:
        if rename_PDF_file(new_name, match_name[new_name]):
            renamed = True
    return renamed